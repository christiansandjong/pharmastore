package com.smc.pharmastore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PharmastoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(PharmastoreApplication.class, args);
	}

}
