package com.smc.pharmastore.model;

public class Medicament {

    private String nom;
    private float prix;
    private int stock;

    public Medicament(String nom, float prix, int stock) {
        this.nom = nom;
        this.prix = prix;
        this.stock = stock;
    }

    public String getNom() {
        return nom;
    }

    public float getPrix() {
        return prix;
    }

    public int getStock() {
        return stock;
    }

    public String toString() {
        return  nom +" - "+ prix +" - "+ stock +" unités ";
    }

    public void diminueStock(int nb) {
        stock -= nb;
    }
}
