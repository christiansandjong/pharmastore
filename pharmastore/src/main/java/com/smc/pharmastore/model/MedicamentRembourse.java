package com.smc.pharmastore.model;

public class MedicamentRembourse extends Medicament{

    private float pourcRemb;

    public MedicamentRembourse(String nom, float prix, int stock, float pourcRemb) {
        super(nom,prix,stock);
        this.pourcRemb =pourcRemb;
    }

    public float getPourcRemb() {
        return pourcRemb;
    }


}
